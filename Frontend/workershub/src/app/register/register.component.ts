import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesService } from '../services.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {
  loginStatus: any;
  countries: any;
  customer: any;
  confirmPassword: string = '';

  constructor(private service: ServicesService, private router: Router, private toastr: ToastrService) {
    this.customer = {
      customerName: '',
      emailId: '',
      password: '',
      phoneNumber: '',
      gender: ''
    };
  }

  ngOnInit() {
    this.service.getLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
    });
  }

  registerSubmit(regForm: any) {
    if (this.customer.password == this.confirmPassword) {
      this.customer.customerName = regForm.customerName;
      this.customer.gender = regForm.gender;
      this.customer.emailId = regForm.emailId;
      this.customer.password = regForm.password;
      this.customer.phoneNumber = regForm.phoneNumber;
      console.log(this.customer);
      console.log(this.customer.pincode);
      this.service.addCustomer(this.customer).subscribe((data: any) => { console.log(data); });
      this.toastr.success('Registration success');
      this.router.navigate(['customerregistrationotp']);
    }
    else {
      console.log('Password and Confirm Password must be the same.');
      this.toastr.error('Registration Failed');
    }
  }
}