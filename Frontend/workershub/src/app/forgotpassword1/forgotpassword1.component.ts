
import { Component } from '@angular/core';
import { ServicesService } from '../services.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgotpassword1',
  templateUrl: './forgotpassword1.component.html',
  styleUrl: './forgotpassword1.component.css'
})
export class Forgotpassword1Component {
  emailId: string = '';
  customers: any;

  constructor(private service: ServicesService, private toastr: ToastrService, private router: Router) {
    this.customers = {
      emailId: '',
      password: ''
    };
  }

  ngOnInit() {
    this.service.currentEmailId.subscribe((emailId: string) => {
      this.emailId = emailId;
    });
  }

  passwordChanged(regForm: any) {
    this.customers.emailId = regForm.emailId;
    this.customers.password = regForm.password;
    console.log(this.customers.emailId);
    console.log(this.customers.password);
    this.service.updateCustomerPassword(this.customers).subscribe((data: any) => { console.log(data); });
    this.toastr.success('password changed');
    this.router.navigate(['login']);
  }

}