import { Component } from '@angular/core';
import { ServicesService } from '../services.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-workersregister',
  templateUrl: './workersregister.component.html',
  styleUrl: './workersregister.component.css'
})
export class WorkersregisterComponent {
  loginStatus: any;
  countries: any;
  worker: any;
  confirmPassword: string = '';

  constructor(private service: ServicesService, private router: Router, private toastr: ToastrService) {
    this.worker = {
      workerName: '',
      emailId: '',
      password: '',
      phoneNumber: '',
      pincode: '',
      doj: '',
      department: ''
    };
  }

  ngOnInit() {
    this.service.getLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
    });
  }

  registerSubmit(regForm: any) {
    if (this.worker.password == this.confirmPassword) {

      this.worker.workerName = regForm.workerName;
      this.worker.pincode = regForm.pincode;
      this.worker.emailId = regForm.emailId;
      this.worker.password = regForm.password;
      this.worker.phoneNumber = regForm.phoneNumber;
      this.worker.doj = regForm.doj;
      this.worker.department = regForm.department;

      console.log(this.worker);
      console.log(this.worker.pincode);

      this.service.addWorkers(this.worker).subscribe((data: any) => { console.log(data); });
      this.toastr.success('Registration success');
      this.router.navigate(['workerregistrationotp']);
    }
    else {
      console.log('Password and Confirm Password must be the same.');
      this.toastr.error('Registration Failed');
    }
  }
}