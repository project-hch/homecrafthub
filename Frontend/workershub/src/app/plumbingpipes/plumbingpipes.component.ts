import { Component } from '@angular/core';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-plumbingpipes',
  templateUrl: './plumbingpipes.component.html',
  styleUrl: './plumbingpipes.component.css'
})
export class PlumbingpipesComponent {
  products: any;
  emailId: any;
  selectedProduct: any;
  cartProducts: any;

  constructor(private service: ServicesService) {
    this.emailId = localStorage.getItem('emailId');
    this.cartProducts = [];
    this.products = [
      {
        id: 501,
        name: 'pipes',
        description: '1 Inch Sudhakar1.2mm Pipes Leveraging the skills of our qualified team of professionals, we are instrumental in offering a wide .',
        price: 2000,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/d/o/download.jpg'
      },
      {
        id: 502,
        name: 'Bend Pipes ',
        description: ' We have highly acknowledged organisation engaged in presenting a remarkable range of Sudhakar PVC Pipe Bend"',
        price: 100,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/l/o/long_bends_3_8.jpg'
      },
      {
        id: 503,
        name: 'Tank adaptor',
        description: 'plumbing component designed for connecting a CPVC water supply line to a water tank or similar container.',
        price: 15000,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/2/0/20_x_15mm_cpvc_brass_tee_1.jpg'
      },
      {
        id: 504,
        name: 'Paint',
        description: 'Indigo Metallic Emulsion is a revolutionary water based paint suitable for interior &exterior application',
        price: 1500,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/p/f/pfdepcinp0004_1_1_1_1.png',
      },
      {
        id: 505,
        name: 'putty',
        description: ' Wall safe putty is a versatile and moldable adhesive designed for securing and concealing small safes or valuables within walls. ',
        price: 1200,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/f/c/fcwp.png'
      },
      {
        id: 506,
        name: 'Enamuel',
        description: 'Asian Paints Apcolite Premium Gloss Enamel - Shades - 1 Ltr - Bus Green  safes or valuables within the walls',
        price: 2000,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/p/f/pfdepcasn0041g_3.jpg'
      },

    ];
  }

  ngOnInit() {
  }

  addToCart(product: any) {
    this.service.addToCart(product);
  }

}

