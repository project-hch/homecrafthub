import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ServicesService } from '../services.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-workersbypincode',
  templateUrl: './workersbypincode.component.html',
  styleUrl: './workersbypincode.component.css'
})
export class WorkersbypincodeComponent implements OnInit {

  pincode: any;
  workers: any[] | undefined;
  formModel: any = {};
  buttonClicked: boolean = false;

  // Dependency Injection for EmpService
  constructor(private service: ServicesService, private router: Router) {
  }

  ngOnInit() {
  }

  getWorkers() {
    this.service.getWorkersByPincode(this.formModel.pincode).subscribe((data: any) => {
      this.workers = Array.isArray(data) ? data : [data];  // Ensure workers is an array
      console.log(this.workers);
    });
  }

  @Output() bookNowClicked: EventEmitter<string> = new EventEmitter<string>();

  bookNow(emailId: string) {
    console.log('Book-Now Clicked. Navigating to Address Component.');
    this.bookNowClicked.emit(emailId);
    this.router.navigate(['/address', { emailId: emailId }]);
  }

}