import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ProductsComponent } from './products/products.component';
import { LogoutComponent } from './logout/logout.component';
import { authGuard } from './auth.guard';
import { WorkersComponent } from './workers/workers.component';
import { WorkersbypincodeComponent } from './workersbypincode/workersbypincode.component';
import { HomeComponent } from './home/home.component';
import { CartComponent } from './cart/cart.component';
import { OtpverificationComponent } from './otpverification/otpverification.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { WorkersloginComponent } from './workerslogin/workerslogin.component';
import { WorkersregisterComponent } from './workersregister/workersregister.component';
import { WorkerregistrationotpComponent } from './workerregistrationotp/workerregistrationotp.component';
import { ConstructionComponent } from './construction/construction.component';
import { ElectricalComponent } from './electrical/electrical.component';
import { FixturesComponent } from './fixtures/fixtures.component';
import { Otpverification1Component } from './otpverification1/otpverification1.component';
import { Forgotpassword1Component } from './forgotpassword1/forgotpassword1.component';
import { FurnitureComponent } from './furniture/furniture.component';
import { PlumbingpipesComponent } from './plumbingpipes/plumbingpipes.component';
import { CustomersComponent } from './customers/customers.component';
import { CustomerregistrationotpComponent } from './customerregistrationotp/customerregistrationotp.component';
import { AddressComponent } from './address/address.component';
import { ContactusComponent } from './contactus/contactus.component';
import { PaymentComponent } from './payment/payment.component';

const routes: Routes = [
  { path: '',                                           component: HomeComponent },
  { path: 'home',                                       component: HomeComponent },
  { path: 'login',                                      component: LoginComponent },
  { path: 'workerlogin',                                component: WorkersloginComponent },
  { path: 'register',                                   component: RegisterComponent },
  { path: 'workersregister',                            component: WorkersregisterComponent },
  { path: 'WorkerregistrationotpComponent',             component: WorkerregistrationotpComponent },
  { path: 'aboutus',                                    component: AboutusComponent },
  { path: 'products',                                   component: ProductsComponent },
  { path: 'contactus',                                  component: ContactusComponent },
  { path: 'construction',     canActivate: [authGuard], component: ConstructionComponent },
  { path: 'electrical',       canActivate: [authGuard], component: ElectricalComponent },
  { path: 'fixtures',         canActivate: [authGuard], component: FixturesComponent },
  { path: 'plumbingpipes',    canActivate: [authGuard], component: PlumbingpipesComponent },
  { path: 'fixtures',                                   component: FixturesComponent },
  { path: 'logout',                                     component: LogoutComponent },
  { path: 'otpverification',                            component: OtpverificationComponent },
  { path: 'forgotpassword',                             component: ForgotpasswordComponent },
  { path: 'otpverification1',                           component: Otpverification1Component },
  { path: 'Forgotpassword1',                            component: Forgotpassword1Component },
  { path: 'workers',         canActivate: [authGuard],  component: WorkersComponent },
  { path: 'customers',                                  component: CustomersComponent },
  { path: 'workersbypincode', canActivate: [authGuard], component: WorkersbypincodeComponent },
  { path: 'workerregistrationotp',                      component: WorkerregistrationotpComponent },
  { path: 'customerregistrationotp',                    component: CustomerregistrationotpComponent },
  { path: 'cart',                                       component: CartComponent },
  { path: 'address',                                    component: AddressComponent },
  { path: 'FurnitureComponent',canActivate: [authGuard],component: FurnitureComponent },
  {path:'payment',                                      component:PaymentComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
