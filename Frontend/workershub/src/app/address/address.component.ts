import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrl: './address.component.css'
})
export class AddressComponent implements OnInit {
  selectedEmailId: string = '';
  selectedAddress: string = '';

  constructor(private router: Router, private route: ActivatedRoute, private service: ServicesService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.selectedEmailId = params['emailId'];
    });
  }

  submitForm() {
    const workerDetails = {
      emailId: this.selectedEmailId,
      address: this.selectedAddress,
    };

    this.service.address(workerDetails).subscribe((response: any) => {
      console.log(response);
      this.router.navigate(['home']);
    });
  }

}