import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent implements OnInit {

  emailId: any;
  total: number;
  products: any;

  constructor(private service: ServicesService, private router: Router) {
    this.total = 0;
    this.emailId = localStorage.getItem('emailId');
    this.products = service.getCartItems();

    this.products.forEach((element: any) => {
      this.total += parseInt(element.price);
    });
  }

  ngOnInit() { }

  deleteCartProduct(product: any) {
    const i = this.products.findIndex((element: any) => element.id == product.id);
    this.products.splice(i, 1);
    this.total -= product.price;
  }

  purchase() {
    this.router.navigate(['payment', { amount: this.total }]);
    this.service.setCartItems();
    this.total = 0;
  }
}