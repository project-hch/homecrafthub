import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrl: './payment.component.css'
})
export class PaymentComponent implements OnInit {


  amount: any;

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.amount = params['amount'];
    });
    this.loadStripe();
  }

  pay() {
    if (this.amount) {
      var handler = (<any>window).StripeCheckout.configure({
        key: 'pk_test_51HxRkiCumzEESdU2Z1FzfCVAJyiVHyHifo0GeCMAyzHPFme6v6ahYeYbQPpD9BvXbAacO2yFQ8ETlKjo4pkHSHSh00qKzqUVK9',
        locale: 'auto',
        token: (token: any) => {
          console.log(token);
          alert('Payment Success!!');
          this.router.navigate(['home']);
        }
      });

      handler.open({
        name: 'Demo Site',
        description: 'Payment for Cart Items',
        amount: this.amount * 100
      });
    } else {
      alert('Invalid Amount');
    }
  }

  loadStripe() {
    if (!window.document.getElementById('stripe-script')) {
      var s = window.document.createElement('script');
      s.id = 'stripe-script';
      s.type = 'text/javascript';
      s.src = 'https://checkout.stripe.com/checkout.js';
      s.onload = () => {
        // Stripe Checkout configuration
      };

      window.document.body.appendChild(s);
    }
  }
}
