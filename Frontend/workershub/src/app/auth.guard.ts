import { CanActivateFn } from '@angular/router';
import { ServicesService } from './services.service';
import { inject } from '@angular/core';

export const authGuard: CanActivateFn = (route, state) => {
  let service = inject(ServicesService);
  return service.getIsUserLogged();
};
