import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrl: './customers.component.css'
})
export class CustomersComponent implements OnInit {
  customers: any;
  emailId: any;

  constructor(private service: ServicesService, private toastr: ToastrService) {
    this.emailId = localStorage.getItem('emailId');
  }

  ngOnInit() {
    this.service.getAllCustomers().subscribe((data: any) => {
      console.log(data);
      this.customers = data;
    });
  }

  deleteCustomer(customer: any) {
    this.service.deleteCustomerById(customer.customerId).subscribe((data: any) => {
      console.log(data);
    });

    const i = this.customers.findIndex((element: any) => {
      return element.customerId === customer.customerId;
    });

    this.customers.splice(i, 1);
    this.toastr.success('Customer Deleted Successfully!!!');
  }
}
