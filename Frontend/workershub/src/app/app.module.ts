import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { RouterModule } from '@angular/router';
import { NgxCaptchaModule } from 'ngx-captcha';
import { NavbarComponent } from './navbar/navbar.component';
import { RegisterComponent } from './register/register.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ProductsComponent } from './products/products.component';
import { LogoutComponent } from './logout/logout.component';
import { WorkersComponent } from './workers/workers.component';
import { WorkersbypincodeComponent } from './workersbypincode/workersbypincode.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { WorkersloginComponent } from './workerslogin/workerslogin.component';
import { WorkersregisterComponent } from './workersregister/workersregister.component';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { CartComponent } from './cart/cart.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { OtpverificationComponent } from './otpverification/otpverification.component';
import { WorkerregistrationotpComponent } from './workerregistrationotp/workerregistrationotp.component';
import { ConstructionComponent } from './construction/construction.component';
import { ElectricalComponent } from './electrical/electrical.component';
import { FixturesComponent } from './fixtures/fixtures.component'
import { Forgotpassword1Component } from './forgotpassword1/forgotpassword1.component';
import { Otpverification1Component } from './otpverification1/otpverification1.component';
import { AddressComponent } from './address/address.component';
import { FurnitureComponent } from './furniture/furniture.component';
import { PlumbingpipesComponent } from './plumbingpipes/plumbingpipes.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { CustomerregistrationotpComponent } from './customerregistrationotp/customerregistrationotp.component';
import { CustomersComponent } from './customers/customers.component';
import { ExpPipe } from './exp.pipe';
import { ContactusComponent } from './contactus/contactus.component';
import { PaymentComponent } from './payment/payment.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    RegisterComponent,
    AboutusComponent,
    ProductsComponent,
    LogoutComponent,
    WorkersComponent,
    WorkersbypincodeComponent,
    HomeComponent,
    FooterComponent,
    WorkersloginComponent,
    WorkersregisterComponent,
    CartComponent,
    ForgotpasswordComponent,
    OtpverificationComponent,
    WorkerregistrationotpComponent,
    ConstructionComponent,
    ElectricalComponent,
    FixturesComponent,
    Forgotpassword1Component,
    Otpverification1Component,
    AddressComponent,
    FurnitureComponent,
    PlumbingpipesComponent,
    CustomerregistrationotpComponent,
    CustomersComponent,
    ExpPipe,
    ContactusComponent,
    PaymentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    RouterModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    MatMenuModule,
    MatButtonModule,
  ],
  providers: [
    provideAnimationsAsync()
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
