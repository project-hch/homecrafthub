import { Component } from '@angular/core';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrl: './contactus.component.css'
})
export class ContactusComponent {
  Contact: any = {
    emailId: '',
    subject: '',
    message: ''
  };

  constructor(private service: ServicesService) { }

  contactUs() {
    this.service.message(this.Contact).subscribe((data: any) => {
      console.log(data);
      this.Contact = {
        emailId: '',
        subject: '',
        message: ''
      };
    });
  }
}
