import { Component } from '@angular/core';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-furniture',
  templateUrl: './furniture.component.html',
  styleUrl: './furniture.component.css'
})
export class FurnitureComponent {
  products: any;
  emailId: any;
  selectedProduct: any;
  cartProducts: any;

  constructor(private service: ServicesService) {
    this.emailId = localStorage.getItem('emailId');
    this.cartProducts = [];
    this.products = [
      {
        id: 401,
        name: 'plywood',
        description: 'Plywood is a versatile engineered wood product composed of thin layers of wood veneer glued together, providing strength and durability.',
        price: 2000,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/w/p/wpplywcyp0006.jpg'
      },
      {
        id: 402,
        name: 'shuttering plywood ',
        description: ' shuttered plywood is a wood  that enable the movement and rotation of doors around a fixed axis, facilitating their opening and closing.',
        price: 100,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/s/h/shutteringplywood.jpg'
      },
      {
        id: 403,
        name: 'WPC Door',
        description: ' WPC Wood Plastic Composite door is a modern alternative to traditional wooden doors combining wood fibers and thermoplastics ',
        price: 15000,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/w/p/wpc-solid-doors.jpg'
      },
      {
        id: 404,
        name: 'FLUSH DOORS',
        description: 'flush door is a modern alternative to traditional wooden doors combining wood fibers and thermoplastics to create a durable',
        price: 899,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/f/l/flash-door.jpg',
      },
      {
        id: 405,
        name: 'BlockBoards',
        description: ' traditional wooden doors combining wood fibers and thermoplastics to create a durable weather-resistant ',
        price: 240,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/w/p/wpblbdcyp0003.jpg'
      },
      {
        id: 406,
        name: 'laminates',
        description: 'is a modern alternative to traditional wooden doors combining wood fibers and thermoplastics to create a durable weather-resistant',
        price: 2000,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/w/p/wpdclmvrg0011.png'
      },
      {
        id: 407,
        name: 'window  frame',
        description: 'better in rooms overlooking gardens and terraces. This new-generation door, designed for large.',
        price: 5000,
        imageUrl: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBIVFRgWFBIYGBgaGBoYGBoYGhkYGRgaGhgcGRgYGhwcIS4lHB4rHxoYJjgmKy8xNTU1GiQ7QDs0Py41NTEBDAwMEA8QHBIRHzQhISExMTQ0NDQxMTQ0MTQ0NDQ0NDQ0NDE0NDQ0NDQ0MTQ/PzQ/MTQ0NDExNDE0PzQxMTExMf/AABEIAPcAzAMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAABwEFAwQGAgj/xABQEAABAgMBCAoNCgUDBQAAAAABAAIDBBEhBQYHEjFzstEWIjRBUVRxcpOxExQjMjNTYYGRkqHB0hUXJEJSVWJ0s/BDgqLC4URkgyU1Y8Pi/8QAGAEBAAMBAAAAAAAAAAAAAAAAAAECAwT/xAAhEQEAAgIBBQEBAQAAAAAAAAAAAQIRMQMSEyEyUWFBIv/aAAwDAQACEQMRAD8AcyELBNTDIbHPiPaxjRjOc4hrWgZSScgQZkKm2U3P47AstPdGa1LL55B3ezkA/wDKzWozCcSuEKr2RSXG4HSs1qdkMlxuB0rNaZgxKzQqvZDJcbgdKzWg3wyXHIHSs1pmDErRCq9kMlxuB0jNaNkMlxyB0rNaZMLRCrNkElxuB0jNa9fLspxqD0jNaZgxKxQq43dlONQekZrUfL8nxuB0jNaZgxKyQqvZFI8cgdKzWjZFJcbgdIzWmYMStEKr2QyXG4PSM1o2RSXG4HSs1pmDErRCq9kUlxuB0jNaNkUlxuB0jNaZgxK0QqvZFJcbgdIzWjZFJcbgdIzWmYMStEKo2TSFv0yBZl7oyy2nCtmRutLxiRBjw4hFpxHNdTetoVJiW+hCEQhUd+24JnMv6leKjv23DM5l/UiYIhjBR/kb/c1ZZeWe8gAgWmnq/wCFhae//f1mrBdmA8tDmPc20WAkA137FjPmXVE4hdC5zxa4+wlQ+QdTL5qELkmyEU5Yh9LtaytuXEP8Uj1taYiP6mJmf46RkoCbR7Fl7SIyUXLG5UTxulrXn5KieM0taRj6ic/HWdqnyej/AAvPaRO8FyvyRE8Z161IuK/xnsOtT4+onPx1QkH/AGfQp7SfwFcmbjv8Z7DrXj5JiVpj9etPCPPx2LZN2+D6KoiyJAs9FFy8G4lW1e91fIbPItY3KiW0ecpG/XrU+E/6j+Op7ScN4r02TcuVNy4v2z6Xa1IuVE8Yf6tajEfU5mf46ztI8C9CT8i5QXJieN9rta9/IkQ/xh5ydajx9Wit/jp+0jwLJ2id6i5gXBiePHmLj71BuA/xw/q1qMx9Oi/x1AkXcAy8KxxZYtFfxD21XMvuHEbb2b0Y2tepW5kVr2ExDStagneBOQlT4+q/6jxMLp7bXn8LT/WAu5wR+Ej8xmm5cRMZX80abV2uCR1YsfmM03K1Wd48SaalCFowQqO/TcMzmndSvFQ377gmc09ExsiYf1v3vhTPd75Khecgef3lCyT52gPlCwnbqrpcXlXoy10IsQRy8BjGuGI6lrnEcHkXZHA9c7xkf12/Cq7A4e6zGbh6T02FrWPDC9pi04Lf5nrn+Nj+u34V5dgckPHzA/mZ8KZSFOIV6rfSz+ZuR4zMj+aH8Cj5m5Pempn0w/gTNQmIOq30sDgblONzPpZ8K5O+m9OHc6MxsOM94ezHOPi2HHxfqgJ9pTYXT9Ig5o/qKLR4X47T1Q4tjji5N5eZCSExGZAc9zWviNa4toHAF1tKqQNqLeD21WW9nd0HPM0llWPLe8zh3wwOyXGZn1mfAj5nZHjEz6zPgTLQtsQ5eqS1+ZyR4zM+sz4EfM7I8YmfWZ8CZSExB12+lt8z0lxmZ9aH8Cn5npHjEz67PgTIQnTB12+lPd3BdJS8vFjMixy6Gxz2gvbQkCorRoK4kRO8HATy2NKeV+m4ZnMv6kiWWvHK7RKpeI8NuG04ltRXWP5jdNq7XBGe6x+YzTcuIyh/MGm1dtgi8LHzbNNyiv8ADk1JqoUKVq50Kiv23BM5pyvVRX67gmc0/qRMbInF2jz+64zSpugKMH8vUvTaYj/3viqi6XeC37PvWE7dVdO5wN+FmM3C0npsJUYHB3WZzcLSemuta6c19pQhCsqEIQghKbC5uiFmT+omylNhc3RCzJ/UVbaX4/ZxTcjfMs17W7YH5hg/qWEHaM5D1rNey76bA/MM61nXbovp9EoQhbOQIQhAIQhBRX67hmcy/qSJgnbjldolPW/c/QJrMv6kioHf+d2iVSzbi1LOT3/MGm1dvgi8LH5jNNy4km13Mbphdtgi8LH5jNNyrXcL31JqKVClauZCor9dwTOaf1K9VJfluGZzT+pROkxuCFZ3juU9YK93QG0HKFjHeP5fepnXDEHKFlLqh32Bvwszm4Wk9NdKfAye6TPMhaT02FpXTmvtKEIVlQhCEEJS4X90Qsyf1E2kpMMPh4WZOmq20vT2cR9RnJRbF7R+mQPzDNJazHVY3kWa9o/TZf8AMM01nXbovp9HIQhbOQIQhAIQoQUV++4JrMv6kioHhPW6intfruCZzL+pIeAe6Dld1FUs249S2A6x5/C3SC7fBD4WPm2ablxBsxxwtGk232ruMEXhY/MbpuVa7Xv6yailCFq5kKjvz3DM5pyvFRX6upIzJ/8AE5ROkxuCHdY19Rv5fPkXiaNWV8oXuLkd6favE0Np6FjLrh3+BjwkzzIWlETZSmwMeEmeZC0oibK1rpzX2lCEKygQhCCEosMI+kQsyf1E3UosMW6IWZP6irbS/H7Q4eH3reT3lZ7292y/5hmmsELvW8nvKz3ubtlz/uIeks67dF9Po9CELZyBCEIBQpQgoL+NwTOackTBHdPW6iU9b+dwTOad7kiYXhB/NolUtttx6lsPNMcfgHoxmhdxgh8LHzbdNy4eILX0t2g02rt8EHho+bZpuVY2tf1NdCELVzoVFfqaSEzmnK9VDfvuCZzTlEpgh4jxb+62rzOd4PMvDfreQHSC9TNrB5usrGXXXRg4Gh3Wa5kHSiJsJVYGxt5k0yshaURNVa105uT2lKEIVlAhCEEJR4YT3eFmD+om4lHhhH0iFmf/AGFVtpfj9ocLANWN5D1lbF7Z+mS/5iGf6lrw2jEbye8rYvb3bA/MM01nDovp9HoUKVs5AhCEAoKlQgoL+dwTOad7kiYTu6t5XaJT1v53BM5o9YSIl/Ct/m6lS223HptQMp5lfQ8LuMEJ7tHzbNNy4aVNruZ1uC7nBH4ePmm6blWu1uT1NZCELVzoVDfvuCZzTlfKhv43BM5pyiUwQbt/lKyRztAPL71iJsPOPtqFlmxtGU/dSsbbdddPUC+eLLNxGRCPJQkC2thBCDf3NeO/UHU9d7geA7NND8ELremrijgC0rGYY2vidPm5l/cyP436h/vRs9m/Hn+v4l9I4g4B6EYg4B6FPSr3Px83tv8AZofxnel/xL1s+mfHH0xPiX0b2Nv2R6Ajsbfsj0BOlPc/Hzm2/wCmK17M70xPjUT18hmm0e6rhTKDjYotpUk2VNaL6N7G37I9ASrwxtAfLUAG0i734mKtoxC1L5tEYcBAqWAW/sqJSe7A4xA4NIdvituNYR5QRlClho1vJb6Sst7lDNywIBHbDMtv11SPLW3iGy/CLM70dw88T414GEac4w70v+NfQfacLxbPVbqR2nC8Wz1W6lrhzdf4+fPnGm/Hu9aJ8S8/OFNH/UP9eIP719DdpwvFs9VupHakPxbPVbqTpT1/j56GECa4zE9d/vcpOEKb40/0v+JfQvasPxbPVGpHa0PxbPVGpOk6/wAfPov6ivaWPmHODrKOx3A2gioLiDaOBaTH90Dq1qXGvDUJ6X5QWCRmSGNsgv3h9lIqHQvHK7qKpaMNKTmGxLmjn8waQXcYIT3aPm26blwrX7Z5/B/c1dzgfPdo+bbpuSuzk9TYQhC1c6FRX67gmc09Xqob99wTWaeonSY2QJNjud71mmm0Yzh/ysRNjud71lmhtG+brWNnXXTv8Dh7tNcyDpRE10p8DfhprmQdKImwta6c3J7SlCEKygQhCCErMMXfy+bi6UNNNKrDIdvLcyLpMVbaX4/aC5ZkHJ7yti9wUnJb8wzTCwwh3vJT2lbF75+mS3kmYemAso26b6fSKlCFu4whCEAoUoQUN+24JrMRNFIWXO3B8p0Sn1ftuCazL+pISF345T1FZ3234tSy1oX+Rn97V3eB492j5tum5cI60u5n97V3WB7w0fNs03KK7TfRtIQhaudCob99wTOZf1K+VFfruCZzT1E6TG4IB2R3Kssz3jT+8qwnI7na1mmztG/vfWMuurvsDHhZrmQtJ6bKUuBc91muZC0oibS1rpzcntKUIQrKBCEIISqwxnuksPwROtiaqUuGU92ls3E02KttL8XtBfQ8g5K+0rYve3XLn/cw9MLXY7JzT1lbFwD9KlvzUPTCzrt030+lEIQtnGEIQgEIQgoL+NwTWZf1JCM78crtEp9X87gmsw/qSGl6Y4r+LRKzvtvw6bcABz30yBhP9TV22B/w0fNt03LhJN23d5WOHtC7vA/4aPm2/qOVa7W5PU2EIQtnMhUV+m4ZnNOV6qO/PcMzmn9SidJjcPn4mx3OPvWWc7xn731gxu+HlPWtia7xv731hLrq77Ax4Wa5kLSiJspTYGPDTXMg6URNlbV05uT2lKEIVlAhCEEJR4Zj3eWzUXTYm4lFhlH0iWzUTTYq20vxe0F+3I3kPWVnuEPpUv8AmYX6jVrA5OT3lbFxHfSYH5mF+o1ZVdN/6+mEIQt3GEIQgFClCDnr/P8At81mInUkDAdtxyP6k/r+x/0+azL+pfP8E7fzP6lnfbfi02YBoTzT1hd9gePdo+aZpuS+hZTzT1hMHA+O7R80zTcq02tyeptIQhbOZCo79NwzOad1K8VHfpuGZzTlE6TG4fPpNjuU9ZWxNd6z9761ye/5T1lbU6No3kB9JWFnZV3eBg91muZB0oibKUuBjws1zIOk9NpbV05eT2lKEIVlAhCEEJRYZnUmJbNRNNibqUOGUfSJbNxPa9qpfTTi9oL05Rye8rPcY/SYH5mD+o1YCbRyHrKzXJb9IgfmYP6jFnDovG300FKELdxhCEIBCEIOfv7/AO3zWZf1JAQxR/mf1J/387gmc073L5/adv53LK+3Rw6l7hEgup9k6QTEwPHu0fNs03Jds+t5GHTamHgc8LHzbdNyim08nqbSEIWzmQqO/TcMzmnK8VDfvuCZzT+pROkxuCAc2x58p61tzMMua1rRU0FgtPfcAWkHCkQeXWrKFHfDcx8N4a8N2jqVA2jsYEcNCaLCduyNO3wYFsuYz4oLA8Ma2rTU4peTZvZV3+yKW+2fVdqSRdd26ByzLfJtP8IddmfFpjinMb8KtFpiPDK1ImcyduyKVydkPqnf8yjZLK1p2S3kKSJutNu/1X9Pl5q9G780LBEFm/iu3vMp6pR26/p1m+aV+2fVdqUbKZT7bvUfqSTbfJNAVMz6eHkxORejfNN70dvKWk+ylidUnbg623zSp+u71XakusJ8Zkd8KLDq5rGlhNCLXOqBQ2nIuZbfLOH+O31dTVmhXxzldtHYOVh+FRNpmMSmtIrOYUcKpLbMoPB9orZuVKv7Yh2HaRWPdYcjHsJI4bAVlhy8NpJMyDUlwqx9hJqQKb1arfl7448FjYbI0MtaKNxobgaVJtqPKq4nPhpa2YOo3wSo/ij0O1Ly2+KUOSKPQ7UkvsvmfGQ/UdqWJt901Xv4XqHUtOqWHRB3bIZTxzfbqRsglPHN9upJQ34TPDC9U/CjZbNeMh+gWf0J1Snog7NkEr45vt1Kfl+V8c326klRfZN+Mgnzf/Kxm++cG/BP8v8AhR1SduDUvrurLxpOOyHFa5zmFrQK1JJAASUMMiKR5Ig4bQLepXbL75rgheizqWGcu7MR24kXExA4OrDaA+oqGjGpYDWh8hVbTMtKR0qOB9fmf3tTFwNHusbNt0yl42GW1qMrf7hYmHgbHdZjNs03KabOX1NtCELZyoXPX+D/AKdNZh/UuhWGZl2RGOZEYHNcC1zXCrXA5QQcoRMTiXzhc7sTHY8UYwJriNcGnlceDyK0iXRueTuR3k7q4eyqcew65n3fLdEzUjYdcz7vluiZqVJplr3SbbP3P35N5/5XH3rMyeudxR7Rvnsjtdqb+w65n3fLdEzUjYbcz7vluiZqUds7v4UDbsSIySrqZLYrvcvTrqSYrSUPmimnWm5sOuZ93y3RM1I2HXM+75bomak7Z3ShF0ZQ/wCjs3z2VxKl87JjJKeiK4Hy76b2w+5nEJbomakbELmcQluiZqUdtPdj4ULp2T3pV3ninWoN0pM2GUPSHWm/sPuZxCW6JmpGxC5nEJbomak7c/Tux8JrtyS4kekOtSJuRrbInpHfEnJsOuZ93y3RQ9SNh1zPu+W6JmpT0fqO7Hwm3TVz9+Qd67viUGduaLe0XU8sR3xJym8+5vEJbomalGw65nEJbomak6DuQTbLoXLI3GfPEdrXn5RuZxJ1h+274k59h1zPu+W6JmpGw65n3fLdEzUnR+nd/CXN0Lm8SPSO+JeXXRkRkka8sR496dWw25n3fLdEzUo2G3M+75bomalPQd38Jdl0JEi2Sp/yv1rKJ+QFolDvfxH73nTj2G3M4hL9EzUjYdcz7vluiZqUdB3SWn7oysRhDJZzD9rHLqeShyrrMDju7xx/4mWcG3K7/Yjc3iEt0UPUtq51xJWXJdAloUIuFHGGxrCQDUA4ottU1rhFuSLRhZoQhXZBCEIBCFCCUKrnbqNY50NrXF4huiVsxWgB1C6pBIq2lgK1rn3fY8Q2ua5r3gVBbS3sRi42U0aQ11N+xBeoXNPvlNHubLRXND4LWnaDGEYsAyvqDt7PNkWU31yuO9gcS+G1znNaWOdtXNa4YodjAguGUCu9VB0CFRTF3w1sUtgxC6E0FzTiChIacU7bLRwPAbaE0WR934TScZr2gHFc5zRitfiY/YyQTtqHeqK2VqguULnzd8CPDYWOaIkNzmscAIjnBzAC0B1KYriTwUtorCZui1j2sxXOcWlxxQCGMBpjPJIoK+ew8CCwQqMXxQrKQ4pLsUsGKKxGuriuZtqUs36HJZati5914cauKHijWvBc3FxmOxsV7bcm1OWh9IQWiFUXQuqWyrpiA0RR2MxGVJY0tDS+pJFQKDgraomrrYsSGxrQQXARSTQQw5jnt5XbXJvC3gQXCFzMxfBEEGYisghwhw+yQ8Z1OyMxHOx3WbUHENBaaEZK2e7p3yCDFxHBlBi1aXd0cHNc5zobfrNaGmp8hQdGhc0+7kdrRjQRjObDeMQRIga2JjVxmsaXEtxaVApaFlh3xQjEhsxmlrmBzogD+x4zxtGNfTFDjQmjiDk3yg6BC56Vu6YzHPhBgo7G7pjsxIWK4tiuDmgkOxbKWUOWwq2ufMGJCY9zC0uaHFp3qiqDbQhCAQhCAQhCAQhCCunLlw4r2ueXHFqWtrRoJY5hdkrXFe4Zd9YIlwIJIO2BaGgOa6hAax0MDJvte4FCEALgwQ0tDnhpEOwOyGEWmG8VHfDEb6F7+R4dHjHfiPqS3G2oJcHEgUsJI9p4VKEETVx4cRznvLyXQ3Q++71ri0uDbKipa3hyLxEuFBdXGxnAnGLXO2rn4mLjkAd9i08lbaVQhBPyHCJBc57nNGK1znVcBjMcKECyhY328JWWLcxrnB5c/GAc0kEDGaTjYjrLQDk/yVKEGGBcKE1zXAvJZihmM6uK1tcVg/Dby5LbFsSNy4cKmJXaw2whU12rCS3z2m1QhBkn5RsaG6G4uDXtLXYpAJBFCKkb4K04170q/FdEgMiPaQceIxjnuoMUY5xbRTqHAhCDzEvblS2K1kNsMRmYkTsTWsLmUIpY3fDjVbfyewlpeXPIa5gxyCKO74kAAVIsrwIQg14FxIbAQyJFBo1odj1cGNBDWAkd6Kny+VeW3vwBQAOxRikw67Vxb3jnDfIy5coBUoQY3XvQi3Fx4thZbjg7VlsNhqKFoJrQg25aq3gsxQBjF1BlNKnloAEIQZUIQgEIQg//2Q==',
      },
      {
        id: '408',
        name: 'veeners',
        description: 'wooden doors combining wood fibers and thermoplastics to create a durable ',
        price: 140,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/w/p/wpvnrgpi053.png',
      },
      {
        id: 409,
        name: 'foldeable window mirror',
        description: 'it is a flexible wood  of material, often adhesive-coated, used for binding, sealing.',
        price: 10000,
        imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRm80uUMu_Np4rGFQMepyUOlJj5peosNq9ezQ&usqp=CAU'
      }
    ];
  }

  ngOnInit() {
  }

  addToCart(product: any) {
    this.service.addToCart(product);
  }

}
