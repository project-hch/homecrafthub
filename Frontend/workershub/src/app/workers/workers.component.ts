import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-workers',
  templateUrl: './workers.component.html',
  styleUrl: './workers.component.css'
})
export class WorkersComponent implements OnInit {
  workers: any;
  emailId: any;

  constructor(private service: ServicesService, private toastr: ToastrService) {
    this.emailId = localStorage.getItem('emailId');
  }

  ngOnInit() {
    this.service.getAllWorkers().subscribe((data: any) => {
      console.log(data);
      this.workers = data;
    });
  }

  deleteWorker(worker: any) {
    this.service.deleteWorkersById(worker.workerId).subscribe((data: any) => {
      console.log(data);
    });

    const i = this.workers.findIndex((element: any) => {
      return element.workerId === worker.workerId; // Fix comparison operation
    });

    this.workers.splice(i, 1);
    this.toastr.success('Worker Deleted Successfully!!!');
  }
}