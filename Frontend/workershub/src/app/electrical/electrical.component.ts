import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-electrical',
  templateUrl: './electrical.component.html',
  styleUrl: './electrical.component.css'
})
export class ElectricalComponent implements OnInit {
  products: any;
  emailId: any;
  selectedProduct: any;
  cartProducts: any;

  constructor(private service: ServicesService) {
    this.emailId = localStorage.getItem('emailId');
    this.cartProducts = [];
    this.products = [
      {
        id: 301,
        name: 'DoorHndles',
        description: 'Door handles are functional and decorative fixtures attached to doors, allowing users to open or close them',
        price: 2053,
        imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToTM0arDRVx8h1pLf5hQgIRJbWEgWImxCgOA&usqp=CAU'
      },
      {
        id: 102,
        name: 'switch',
        description: 'Havells 1 M Combined Plate Havells Life Line Plus Sq.mm  Core HRFR Flexible House Cable white color',
        price: 20,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/200x200/9df78eab33525d08d6e5fb8d27136e95/e/l/eleothhvl0096-104.jpg'
      },
      {
        id: 103,
        name: 'HDMISOCKET ',
        description: ' It supports the transfer of uncompressed and encrypted content, delivering a seamless and high-definition experience. ',
        price: 890,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/e/l/eleothhvl0138.png'
      },
      {
        id: 104,
        name: 'MCB',
        description: 'MCB Combined Plate Havells Life Line Plus3q.mm 1 Core HRFR Flexible House mcb white color',
        price: 899,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/e/l/eleothhvl0027.png'
      },
      {
        id: 105,
        name: 'UNIVERSAL SOCKET',
        description: 'Universal socket Combined Plate Havells Life Line Plus3q.mm 1 Core HRFR Flexible House socket white color',
        price: 140,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/a/h/ahfkuxw133.png'
      },
      {
        id: 106,
        name: 'Havell Modular Plate',
        description: 'Havell Modular Plate is a versatile and aesthetically designed electrical switch plate, known for its modern style and functionality. ',
        price: 26000,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/e/l/eleothhil0102.jpg'
      },
      {
        id: 107,
        name: 'light',
        description: 'CFL 45W DF 4U 6500K (B22/E27) light  known for its modern style and functionality. With two modular lines',
        price: 899,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/c/f/cfl_4.jpg',
      },
      {
        id: '108',
        name: 'Fan',
        description: 'Universal socket Combined Plate Havells Life Line Plus3q.mm 1 Core HRFR Flexible House socket white color',
        price: 140,
        imageUrl: 'https://m.media-amazon.com/images/I/61CwkCxvECL._AC._SR360,460.jpg',
      },
      {
        id: 109,
        name: 'greayser',
        description: 'Activa Amazon 10 L Instant 3 KVA (0.8mm) Special Anti Rust Coated Tank Geyser with 5 Year Warranty, Abs Top Bottom, (IVORY),Wall',
        price: 26000,
        imageUrl: 'https://m.media-amazon.com/images/I/31OJAht2y+S.jpg'
      }
    ];
  }

  ngOnInit() {
  }

  addToCart(product: any) {
    this.service.addToCart(product);
  }

}
