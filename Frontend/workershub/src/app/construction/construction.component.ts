import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-construction',
  templateUrl: './construction.component.html',
  styleUrl: './construction.component.css'
})
export class ConstructionComponent implements OnInit {
  products: any;
  emailId: any;
  selectedProduct: any;
  cartProducts: any;

  constructor(private service: ServicesService) {
    this.emailId = localStorage.getItem('emailId');
    this.cartProducts = [];
    this.products = [
      {
        id: 201,
        name: 'BRICK',
        description: 'Rajahmundry Light Weight Red Bricks - 9in x 4in x 3in',
        price: 15.00,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/200x200/9df78eab33525d08d6e5fb8d27136e95/3/_/3_1.png'
      },
      {
        id: 202,
        name: 'ACC BRICKS',
        description: 'Greenstone  AAC- Brick -600mmX200mmX100mm4 Limestone ',
        price: 35.00,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/b/r/brick_image.png'
      },
      {
        id: 203,
        name: 'CEMENT ',
        description: ' PPC Cement.PPC Cement is used for brickworks, plastering and all other not structural cement,',
        price: 430,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/j/a/jaypee_cement.jpg'
      },
      {
        id: 204,
        name: 'BINDING WIRE',
        description: 'it is a type of wire commonly used for various applications, particularly in construction, gardening, and packaging.',
        price: 899,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/200x200/9df78eab33525d08d6e5fb8d27136e95/b/l/black-binding-wire-250x250.jpg',
      },
      {
        id: 205,
        name: 'TMT BARS',
        description: 'Universal socket Combined Plate Havells Life Line Plus3q.mm 1 Core HRFR Flexible House socket white color',
        price: 140,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/t/m/tm500gsug0001.jpg'
      },
      {
        id: 206,
        name: 'REBAR COUPLER',
        description: 'Spliced rebar performs like continuous reinforcement due to the mechanical joint, unlike lapping which has a complete dependency',
        price: 183,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/c/u/cuppler_1_1_1.jpg',
      },
      {
        id: 107,
        name: 'Tiles',
        description: 'Johnsons Alberto Lemon Douible Charge Tiles -600mm x 600mm Tiles ',
        price: 55,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/200x200/9df78eab33525d08d6e5fb8d27136e95/s/a/sandune_crema_1.jpg',
      },
      {
        id: '108',
        name: 'granite',
        description: 'Finish - Leather, Hand Polish, Flaming, Lapotra and Line Polish Stone From - Gangsaw and Cutter',
        price: 2000,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/s/a/sapphire_blue.jpg',
      },
      {
        id: 109,
        name: 'Generic Agregate',
        description: 'Stone Aggregates for construction 6mm, 10mm, 20mm, 40mm , 60mm and 90mm Packaging.',
        price: 1500,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/a/g/aggregates---20-mm_1.jpg'
      }
    ];
  }

  ngOnInit() {
  }

  addToCart(product: any) {
    this.service.addToCart(product);
  }

}
