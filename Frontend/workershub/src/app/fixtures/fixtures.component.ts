import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-fixtures',
  templateUrl: './fixtures.component.html',
  styleUrl: './fixtures.component.css'
})
export class FixturesComponent implements OnInit {
  products: any;
  emailId: any;
  selectedProduct: any;
  cartProducts: any;

  constructor(private service: ServicesService) {
    this.emailId = localStorage.getItem('emailId');
    this.cartProducts = [];
    this.products = [
      {
        id: 301,
        name: 'DoorHandles',
        description: 'Door handles are functional and decorative fixtures attached to doors, allowing users to open or close them',
        price: 2053,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/g/a/galaxy-dorset_5.jpg'
      },
      {
        id: 302,
        name: 'door  Highes ',
        description: 'Door hinges are mechanical devices that enable the movement and rotation of doors around a fixed axis, facilitating their opening and closing.',
        price: 20,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/r/a/railwayhighes-dorset_3.jpg'
      },
      {
        id: 303,
        name: 'Dorset Electronic Safe Shield ',
        description: ' Godrej Safe Security Locker is a robust and secure storage solution designed to protect valuable items, documents, or cash. ',
        price: 15000,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/e/l/electronicsafe-dorset_12.jpg'
      },
      {
        id: 304,
        name: 'Door pullers',
        description: 'Quba On pull Door Lock QX-2002-PL-CPCopper Plate On pull QX-2002-PL-CP Brass',
        price: 899,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/m/a/marquis_pcs_-dorset.jpg'
      },
      {
        id: 305,
        name: 'window frames',
        description: '2T-2P SLIDING WINDOW - 1220 x 1220 window frames ',
        price: 140,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/u/p/upvc_windows1.jpg'
      },
      {
        id: 306,
        name: 'Ventilator 450 x 889',
        description: 'ventilator 450 *899 is a versatile and aesthetically designed electrical switch plate. ',
        price: 26000,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/v/e/ventilator.jpg'
      },
      {
        id: 307,
        name: 'door frame',
        description: 'better in rooms overlooking gardens and terraces. This new-generation door, designed for large.',
        price: 89999,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/u/p/upupdofen0003.jpg',
      },
      {
        id: '308',
        name: 'TowerBolt',
        description: 'Dorset SS Tower Bolt - with Screw - TS-810-8 INCH - Square Shape, Rod Dia - 10mm',
        price: 140,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/150x150/9df78eab33525d08d6e5fb8d27136e95/t/o/tower_bolt_1.jpg',
      },
      {
        id: 309,
        name: 'Generic Brown/Shuttering Tape',
        description: 'Tape is a flexible strip of material, often adhesive-coated, used for binding, sealing.',
        price: 10,
        imageUrl: 'https://cdn-media.buildersmart.in/media/catalog/product/cache/1/image/200x200/9df78eab33525d08d6e5fb8d27136e95/s/h/shuttering_tape.jpg'
      }
    ];
  }

  ngOnInit() {
  }

  addToCart(product: any) {
    this.service.addToCart(product);
  }

}
