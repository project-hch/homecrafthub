import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServicesService } from '../services.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-workerslogin',
  templateUrl: './workerslogin.component.html',
  styleUrl: './workerslogin.component.css'
})
export class WorkersloginComponent {
  protected aFormGroup: FormGroup;
  isCaptchaValid: boolean = false;
  captchaResolved: boolean = false;
  siteKey: string = "6LfTj2gpAAAAABa17_-TWUraXZG9HfTsmIAFCkmQ";
  loginStatus: any;
  emp: any;

  constructor(private router: Router, private service: ServicesService, private formBuilder: FormBuilder, private toastr: ToastrService) {
    this.aFormGroup = this.formBuilder.group({
      recaptcha: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.service.getLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
    });
  }

  onCaptchaResolved(event: any) {
    this.isCaptchaValid = true;
    this.captchaResolved = true;
  }

  async loginSubmit(loginForm: any) {
    if (loginForm.emailId == 'admin@gmail.com' && loginForm.password == 'admin123') {
      this.service.setIsUserLoggedIn();
      // localStorage.setItem("emailId", loginForm.emailId);
      this.router.navigate(['workers']);
    } else {
      this.emp = null;
      await this.service.workersLogin(loginForm.emailId, loginForm.password).then((data: any) => {
        console.log(data.emailId);
        console.log(data.password);
        this.emp = data;
      });

      if (this.emp != null) {
        this.service.setIsUserLoggedIn();
        localStorage.setItem("emailId", loginForm.emailId);
        this.toastr.success('Login Successful', 'Success');
        console.log("success");
        this.router.navigate(['home']);
      } else {
        this.toastr.error("Invalid Credentials");
        console.log("Invalid")
      }
    }
  }
}