package com.model;

public class Address {
	
	private String address;
	private String emailId;

	public Address() {
	}

	public Address(String address, String emailId) {
		super();
		this.address = address;
		this.emailId = emailId;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
}