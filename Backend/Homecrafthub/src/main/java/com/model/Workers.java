package com.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Workers {
	
	@Id@GeneratedValue
	private int workerId;
	private String workerName;
	@NotNull
	@Column(name = "phoneNumber", unique = true, nullable = false)
	private String phoneNumber;
	@NotNull
    @Column(name = "emailId", unique = true, nullable = false)
	private String emailId;
	private String password;
	private Date doj;
	private String pincode;
	private String otp;
	private String department;
	
	public Workers() {
		
	}

	public Workers(String workerName, @NotNull String phoneNumber, @NotNull String emailId, String password, Date doj,String pincode, String otp, String department) {
		this.workerName = workerName;
		this.phoneNumber = phoneNumber;
		this.emailId = emailId;
		this.password = password;
		this.doj = doj;
		this.pincode = pincode;
		this.otp = otp;
		this.department = department;
	}

	public int getWorkerId() {
		return workerId;
	}
	
	public String getWorkerName() {
		return workerName;
	}

	public void setWorkerName(String workerName) {
		this.workerName = workerName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		if (!phoneNumber.startsWith("+")) {
			this.phoneNumber = "(+91)" + phoneNumber;
		} else {
			this.phoneNumber = phoneNumber;
		}
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public Date getDoj() {
		return doj;
	}

	public void setDoj(Date doj) {
		this.doj = doj;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
}