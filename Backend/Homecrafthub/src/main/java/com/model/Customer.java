package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Customer {
	
	@Id@GeneratedValue
	private int customerId;
	private String customerName;
	private String gender;
	@NotNull
	@Column(name = "phoneNumber", unique = true, nullable = false)
	private String phoneNumber;
	@NotNull
    @Column(name = "emailId", unique = true, nullable = false)
	private String emailId;
	private String password;
	private String otp;
	
	public Customer() {
		
	}

	public Customer(String customerName, String gender, @NotNull String phoneNumber, @NotNull String emailId,String password, String otp) {
		this.customerName = customerName;
		this.gender = gender;
		this.phoneNumber = phoneNumber;
		this.emailId = emailId;
		this.password = password;
		this.otp = otp;
	}

	public int getCustomerId() {
		return customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		if (!phoneNumber.startsWith("+")) {
			this.phoneNumber = "(+91)" + phoneNumber;
		} else {
			this.phoneNumber = phoneNumber;
		}
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}
}
