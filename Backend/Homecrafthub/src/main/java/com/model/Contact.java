package com.model;

public class Contact {
	
	private String EmailId;
	private String subject;
	private String message;
	
	public Contact() {
	}

	public Contact(String emailId, String subject, String message) {
		this.EmailId = emailId;
		this.subject = subject;
		this.message = message;
	}

	public String getEmailId() {
		return EmailId;
	}

	public void setEmailId(String emailId) {
		EmailId = emailId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
