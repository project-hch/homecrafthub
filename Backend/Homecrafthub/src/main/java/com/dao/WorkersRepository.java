package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.model.Workers;

public interface WorkersRepository extends JpaRepository<Workers, Integer>{
	
	@Query("from Workers where emailId=:emailId")
	Workers findByEmailId(@Param("emailId") String emailId);
	
	@Query("from Workers where pincode = :pincode")
	List<Workers> findByPincode(@Param("pincode") String pincode);
	
	@Query("from Workers where otp =:otp")
	Workers findByOtp(@Param("otp") String otp);
}
